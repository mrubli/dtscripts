#!/bin/sh

set -eu

localedir=$HOME/.config/darktable/lua/locale
languages=$(find . -mindepth 1 -maxdepth 1 -type d -not -name '.*')

test -d $localedir || (echo "Locale directory '$localedir' does not exist." && exit 1)

for lang in $languages; do
	dir=$localedir/$lang/LC_MESSAGES
	mkdir -p $dir
	cp -v $lang/LC_MESSAGES/*.mo $dir/
done
