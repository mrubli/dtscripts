--[[
	open_location plugin for darktable
	Copyright ⓒ 2021 Martin Rubli

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local dt = require "darktable"
local du = require "lib/dtutils"
local dsys = require "lib/dtutils.system"

local gettext = dt.gettext


--
-- Constants and Globals

-- List of all known services.
-- This table is ordered, so that we can correctly deduce the service ID from a combobox's
-- selection index.
-- Service attributes:
--   id:   Internal service ID used in preference storage
--   name: Human-readable service name
--   url:  URL to open when jumping to a photo's location.
--         Expanded variables:
--           ${lat} = latitude
--           ${lon} = longitude
local Services = {
	{
		id = "openstreetmap",
		name = "OpenStreetMap",
		url = "https://www.openstreetmap.org/#map=16/${lat}/${lon}",
	},
	{
		id = "opencyclemap",
		name = "OpenCycleMap",
		url = "https://opencyclemap.org/?zoom=16&lat=${lat}&lon=${lon}",
	},
	{
		id = "outdoors",
		name = "OpenCycleMap (Outdoors)",
		url = "http://opencyclemap.org/?zoom=16&lat=${lat}&lon=${lon}&layers=000B0",
	},
	{
		id = "landscape",
		name = "OpenCycleMap (Landscape)",
		url = "http://opencyclemap.org/?zoom=16&lat=${lat}&lon=${lon}&layers=00B00",
	},
	{
		id = "peakfinder",
		name = "PeakFinder",
		url = "https://www.peakfinder.org/?lat=${lat}&lng=${lon}",
	},
}

-- Plugin ID (darktable-internal)
local PluginId = "open_location"

-- Plugin name (human-readable)
local PluginName = "open location"

-- Preference IDs and defaults
local PrefIdServiceId = "service_id"
local PrefDefaultServiceId = "openstreetmap"

-- IDs used for the darktable API
local ActionIdImageOpen = PluginId .. "_action_image_open"
local EventIdViewChanged = PluginId .. "_event_view_changed"

-- Warn when trying to open more than this number of images
local OpenCountThreshold = 3

-- Global plugin data
local Plugin = {
	script_data               = {},						-- script information for script_manager
	event_registered          = false,					-- true if EventIdViewChanged has been registered
	lighttable_ui_initialized = false,					-- true once we've initialized the lighttable view UI elements
	map_ui_initialized        = false,					-- true once we've initialized the map view UI elements
	widgets                   = {},						-- table of UI widgets
	-- Preferences
	service_id                = PrefDefaultServiceId,	-- ID of the currently selected service
}


--
-- Utility functions

-- Log a message with a plugin-specific prefix
local function log(msg)
	dt.print_log(string.format("[%s] %s", PluginId, msg))
end

-- Placeholder function for text translation
local function _(msgid)
	return gettext.dgettext(PluginId, msgid)
end

-- Expands ${var}-type variables in 'string' with the corresponding values from the 'parameters' table
local function expand_variables(string, parameters)
	return string:gsub('($%b{})', function(word) return parameters[word:sub(3, -2)] or word end)
end

-- Returns an ordered table of all known service names
local function get_service_names()
	local names = {}
	for _, service in ipairs(Services) do
		table.insert(names, service.name)
	end
	return names
end

-- Looks up a service given its ID
local function get_service_from_id(service_id)
	for _, service in ipairs(Services) do
		if service.id == service_id then
			return service
		end
	end
	return nil
end

-- Looks up a service given its index in the 'Services' table
local function get_service_from_index(index)
	for i, service in ipairs(Services) do
		if i == index then
			return service
		end
	end
	return nil
end

-- Locale-independent float-to-string conversion that always uses a '.' as a decimal point
local function float_to_string(f)
	local integral, fractional = math.modf(f)
	return string.format("%d.%d", integral, string.sub(tostring(math.abs(fractional)), 3))
end

-- Expands all variables in a service's URL and returns the result
local function expand_url(service, image)
	return expand_variables(service.url, {
		lat = float_to_string(image.latitude),
		lon = float_to_string(image.longitude),
	})
end

-- Opens the given URL in the default web browser
local function open_url(url)
	log(string.format("opening URL: %s", url))
	browser = "x-www-browser '%s'"
	command = string.format(browser, url)
	log(string.format("running command: %s", command))
	dsys.external_command(command)
end

-- Opens the service URLs for all selected images
local function open_selected_images()
	local images = dt.gui.selection()
	log(string.format("opening %d selected image(s)", #images))

	-- Bail out if too few or too many images are selected
	if #images == 0 then
		dt.print(_("Please select at least one image"))
		return
	end
	if #images > OpenCountThreshold then
		dt.print(string.format(_("Please select at most %d images"), OpenCountThreshold))
		return
	end

	-- Warn if at least one image has no latitude/longitude information
	for _, image in pairs(images) do
		if image.latitude == nil or image.longitude == nil then
			dt.print(_("Selected images without location information will be skipped"))
			break
		end
	end

	-- Open the service URLs for all selected images
	for _, image in pairs(images) do
		if image.latitude ~= nil and image.longitude ~= nil then
			log(string.format("opening: %s (latitude: %f, longitude: %f)", image.filename, image.latitude, image.longitude))

			local service = get_service_from_id(Plugin.service_id)
			log(string.format("selected service: %s (%s: %s)", service.id, service.name, service.url))

			local url = expand_url(service, image)
			open_url(url)
		end
	end
end


--
-- Preferences functions

-- Returns the value of the "service ID" preference
local function get_pref_service_id()
	local service_id = dt.preferences.read(PluginId, PrefIdServiceId, "string")
	if service_id == nil then
		service_id = PrefDefaultServiceId
	end
	return service_id
end

-- Stores the value of the "service ID" preference
local function set_pref_service_id(service_id)
	dt.preferences.write(PluginId, PrefIdServiceId, "string", service_id)
end

-- Sets the given service to be the new selected service
local function set_service(service)
	log(string.format("setting current service: %s", service.name))
	Plugin.service_id = service.id
	set_pref_service_id(service.id)
end

-- Initializes all plugin settings with the values form their preferences
local function init_preferences()
	Plugin.service_id = get_pref_service_id()
end


--
-- UI functions

-- Cleans up the UI
local function destroy_ui()
	-- Destroy lighttable UI
	dt.gui.libs.image.destroy_action(ActionIdImageOpen)
	-- Destroy map UI
	-- As of API 7.0 there is no method to unregister the lib, so all we can do is hide it
	--dt.gui.libs[PluginId].visible = false	-- TODO doesn't seem to work, even in combination with script_data.destroy_method = "hide"
end

-- Creates the UI in the lighttable view
local function init_lighttable_ui()
	if Plugin.lighttable_ui_initialized then
		return
	end
	log("initializing lighttable UI")

	dt.gui.libs.image.register_action(
		ActionIdImageOpen,
		_("open location"),
		function() open_selected_images() end,
		_("Open the selected image's location")
	)

	Plugin.lighttable_ui_initialized = true
end

-- Creates the UI in the map view
local function init_map_ui()
	if Plugin.map_ui_initialized then
		return
	end
	log("initializing map UI")

	-- Prepare the module widgets
	table.insert(
		Plugin.widgets,
		dt.new_widget("combobox") {
			label = _("service"),
			changed_callback = function (w)
				local service = get_service_from_index(w.selected)
				set_service(service)
			end,
			value = 1,
			table.unpack(get_service_names())
		}
	)
	table.insert(
		Plugin.widgets,
		dt.new_widget("button") {
			label = _("open location"),
			clicked_callback = function (_) open_selected_images() end
		}
	)

	-- Create the module
	dt.register_lib(
		PluginId,				-- module name
		_(PluginName),			-- user-visible name
		true,					-- expandable
		false,					-- resetable
		{ [dt.gui.views.map] = { "DT_UI_CONTAINER_PANEL_RIGHT_CENTER", 200} },	-- containers
		dt.new_widget("box") {	-- widget
			orientation = "vertical",
			table.unpack(Plugin.widgets)
		},
		nil,					-- view_enter
		nil						-- view_leave
	)

	Plugin.map_ui_initialized = true
end

-- Initializes the plugin when it is first loaded
local function init_plugin()
	-- Directly initialize part of the UI if the current view requires it
	if dt.gui.current_view().id == "lighttable" then
		init_lighttable_ui()
	elseif dt.gui.current_view().id == "map" then
		init_map_ui()
	end

	-- The remainder of the UI gets initialized when we switch to the given view
	if not Plugin.event_registered then
		-- If we're not in the map view wait until we are
		log(string.format("deferring loading of remaining UI until later (current: %s)", dt.gui.current_view().id))
		dt.register_event(
			EventIdViewChanged,		-- event_name
			"view-changed",			-- event_type
			function(event, old_view, new_view)
				if old_view.id == new_view.id then
					return	-- nothing really changed
				end
				log(string.format("view changed: %s => %s", old_view.id, new_view.id))
				if new_view.id == "lighttable" then
					init_lighttable_ui()
				elseif new_view.id == "map" then
					init_map_ui()
				end
			end
		)
		Plugin.event_registered = true
	end
end


--
-- Startup code

log("plugin loaded")

-- Check API version
du.check_min_api_version("7.0.0", PluginId)

-- Initialize the plugin
gettext.bindtextdomain(PluginId, dt.configuration.config_dir .. "/lua/locale/")
init_plugin()
log("done initializing plugin")

-- Return script information for script_manager
Plugin.script_data.destroy = function()
		-- Integration for script_manager to allow script to be removed without restarting darktable
		dt.destroy_event(EventIdViewChanged, "view-changed")
		destroy_ui()
		log("plugin destroyed")
	end
return Plugin.script_data
