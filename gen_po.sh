#!/bin/bash

lang=${1?Please specify language as first parameter}

proj="open_location"
po_dir="$lang/LC_MESSAGES"

mkdir -p "$po_dir"
xgettext --from-code=utf-8 -l lua -o "$po_dir/$proj.po" "$proj.lua"
