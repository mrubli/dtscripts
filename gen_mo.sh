#!/bin/bash

lang=${1?Please specify language as first parameter}

proj="open_location"
po_dir="$lang/LC_MESSAGES"
mo_dir="$lang/LC_MESSAGES"

mkdir -p "$mo_dir"
msgfmt -v "$po_dir/$proj.po" -o "$mo_dir/$proj.mo"
