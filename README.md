# Darktable plugins

## Open location

### Screenshots

Lighttable view:

![lighttable view](res/lighttable.png)

Map view:

![lighttable view](res/map.png)


### To do

- [ ] Rename
- [ ] Add support for Windows/MacOS
- [ ] UI cleanup

## Development

### Generating and installing a new translation

In the following `xx_XX` is a locale identifier like `en_US` or `de_CH`.

1. Generate a new .po template:
   ```./gen_po.sh xx_XX```
2. Edit all the `msgstr` strings in `xx_XX/LC_MESSAGES/open_location.po`.
3. Generate the machine-readable translation file (.mo):
   ```./gen_mo.sh xx_XX```
4. For testing install the generated .mo file in the location where Darktable expects it (`$HOME/.config/darktable/lua/locale/xx_XX/LC_MESSAGES`).
   You can either do this manually or using the provided script (the script copies all .mo files):
   ```./install_mo.sh```

### Testing a new translation

Start Darktable, if necessary by specifying the locale identifier (also see the [FAQ](https://www.darktable.org/about/faq/#faq-windows-language)):

```darktable --conf ui_last/gui_language=xx_XX```

Note that setting the `LANG` environment variable as suggested in `gettextExample.lua` does not work for me on Linux.
